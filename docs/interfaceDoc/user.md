## 用户模块接口文档  参数格式均为form-data

### 注册

> /weblab/user/register  
> post请求  
> 参数：   
> email 类型string  
> username 类型string  
> password 类型string  
> 返回结果

```json
{
  "err": 0,
  "msg": "success",
  "pkg": null,
  "dbg": null
}
```

### 验证码激活

url: /weblab/user/activation  
请求方式：post  
参数  
code 类型：string 返回结果：

```json
{
  "err": 0,
  "msg": "success",
  "pkg": null,
  "dbg": null
}
```

### 登录

url : /weblab/user/login 请求方式：post 参数：  
username 类型string  
password 类型string  
登录成功返回结果：

```json
{
  "msg": "success",
  "err": 0,
  "pkg": "18d51ae8-4323-4ac0-85b6-306f01ce8ccb"
}
```
载荷里是签发的token，前端将token存起来，此后发请求校验身份用 放在header里，key为access_token
