# 技术栈

## 后端

- [docker-20](https://www.docker.com/)
- [gin](https://github.com/gin-gonic/gin)：Web框架。
- [go-1.17](https://go.dev/)
  - [lib/pq](https://github.com/lib/pq)
  - [go-redis](https://github.com/go-redis/redis)
- [java-17](https://openjdk.java.net/)
- [kubernetes-1.23](https://kubernetes.io/)
- [postgresql-14](https://www.postgresql.org/)
- [python-3](https://www.python.org/)
- [redis-6](https://redis.io/)
- [ubuntu-20](https://ubuntu.com/)：

## Web前端

- [codemirror-6](https://codemirror.net/6/)：代码编辑器。
- [cypress](https://www.cypress.io/)：测试框架。
- [eslint](https://eslint.org/)：代码检查。
- [node-17](https://nodejs.org/)：开发环境。
- [pinia](https://pinia.vuejs.org/)：状态管理。
- [prettier](https://prettier.io/)：代码美化。
- [typescript](https://www.typescriptlang.org/)：开发语言。
- [vite](https://vitejs.dev/)：构建工具。
- [vitest](https://vitest.dev/)：单元测试。
- [volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)：用于Vue开发的VSCode插件。
- [vue-3](https://vuejs.org/)：前端框架。
- [vuerouter](https://router.vuejs.org/)：路由管理。
