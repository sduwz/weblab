# 编码风格

- 空白：空格（SP），不使用制表（HT、TAB）
- 行尾：换行（LF），不使用回车（CR）
- 缩进：2个空格。函数式代码由IDE自动控制。
- 编码：UTF-8。不用签名序列（BOM）。
- WebAPI：
  - 路径固定为三层，例如：`/weblab/user/register`
    - 第一层固定为`/weblab`（方便配置Web服务器反向代理）。
    - 第二层为模块或业务域。
    - 第三层为具体操作名称。
  - 路径各层模式类似`[a-z][a-z0-9_]*]`，只小写字母，下划线分隔单词（不用连字符）。
    - 正例：`change_password`
    - 反例：`change-password`，`3D`。
  - 请求**仅用POST方式**，不使用GET、PUT、DELETE等方法（简化操作，方便调试）。
    - 动作类型在API名称中体现。例如：`/weblab/user/change_password`
  - 响应**仅用JSON格式**，固定结构：

    ```json
    {
      "err": 0,     // 0为正常，可使用msg和pkg属性。非0为异常。
      "msg": "ok",  // err非0时提供可向用户展示的提示信息。
      "pkg": null,  // 载荷，视API而定。
      "dbg": null   // 调试信息，不适宜向用户展示。
    }
    ```

  - 状态不用HTTP响应的`status_code`。
- Go代码可参考Uber指南，其他参考Google编码风格指南，
