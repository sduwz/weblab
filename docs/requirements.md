# WebLab系统需求文档

## 登录

1. 邮箱登录  
1. 用户名登录（username 2-32 密码：至少8位 4类至少三类 大小写、数字、符号)后端校验  
1. 密码重置：邮箱 验证码随机6位 有效期1h放redis 设置新密码     

用户注册：/weblab/user/register   
用户登录：/weblab/user/login  
用户登出：/weblab/user/logout  
忘记口令：/weblab/user/forget_password  
重设口令：/weblab/user/reset_password  
更新口令：/weblab/user/update_password   

## 注册

1. 邮箱必须 密码 用户名（昵称-唯一）
2. 使用redis保存用户信息，签发token 放请求header里 access_token;

## 后端不同语言通信

后端不同语言开发的模块之间使用http请求。

1. 口令强度测算 /weblab/tool/password_strength
   验证后端多语言的互操作性。

前端上传项目只允许zip压缩包，不接受RAR。

## 前后端交互

1. 参数格式默认为JSON
