package cn.sdu.weblab.util;

import cn.sdu.weblab.entity.SysUser;
import cn.sdu.weblab.entity.User;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.security.Security;

@Component
public class UserUtil {
    /**
     * 返回当前登录用户
     *
     * @return
     */
    public static User getCurrentUser() {
        return (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * @param n n位数字的验证码 随机生成
     * @return
     * @throws Exception
     */
    public static String getVerifyCode(int n) throws Exception {
        if (n < 1) {
            throw new Exception("验证码长度错误");
        }
        String str = "";
        for (int k = 0; k < 6; k++) {
            str += (int) (Math.random() * 10);
        }
        return str;
    }
}
