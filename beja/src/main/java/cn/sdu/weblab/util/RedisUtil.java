package cn.sdu.weblab.util;

import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.io.*;

@Service
public class RedisUtil {
    private static Jedis jedis = null;
    //    @Value("${spring.redis.host}")
    private static String host = "123.56.220.39";
    //    @Value("${spring.redis.port}")
    private static int port = 6379;
    //    @Value("${spring.redis.username}")
    private static String username = "root";
    //    @Value("${spring.redis.password}")
    private static String password = "911ABCabc+";

    static {
        jedis = new Jedis(host, port);
        jedis.auth(password);
        jedis.select(1);
    }


    public static byte[] toByteArray(Object o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        byte[] byteArray = baos.toByteArray();
        return byteArray;

    }

    public static void setString(String key, String val) {
        jedis.set(key, val);
    }

    public static void setString(String key, String val, int second) {
        jedis.set(key, val);
        jedis.expire(key, second);
    }

    public static String getString(String key) {
        return jedis.get(key);
    }

    public static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object o = ois.readObject();
        return o;
    }

    public static void setObject(String key, Object o) throws IOException {
        byte[] byteArray = toByteArray(o);
        jedis.set(key.getBytes(), byteArray);
    }

    public static void setObject(String key, Object o, int second) throws IOException {
        byte[] byteArray = toByteArray(o);
        jedis.set(key.getBytes(), byteArray);
        jedis.expire(key.getBytes(), second);
    }

    public static Object getObject(String key) throws IOException, ClassNotFoundException {
        byte[] bytes = jedis.get(key.getBytes());
        if (bytes == null) {
            return null;
        }
        return toObject(bytes);
    }

    public static void delete(String key) {
        jedis.del(key);
        jedis.del(key.getBytes());
    }
}
