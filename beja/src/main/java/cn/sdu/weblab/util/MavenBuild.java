package cn.sdu.weblab.util;

import org.apache.maven.shared.invoker.*;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * maven项目打包工具类
 */
public class MavenBuild {
    /**
     * 打包项目 会在项目根目录下生成target文件
     *
     * @param projectRootPath 要打包的项目的根路径
     * @param mavenPath       要使用的maven路径
     */
    public static void build(String projectRootPath, String mavenPath) {
        InvocationRequest request = new DefaultInvocationRequest();
        //设置根路径
        request.setBaseDirectory(new File(projectRootPath));
        //设置要执行的maven操作
        request.setGoals(Arrays.asList("clean", "package"));
        //调用工具
        Invoker invoker = new DefaultInvoker();
        //设置要使用的maven路径
        invoker.setMavenHome(new File(mavenPath));
        try {
            //开始执行maven构建
            InvocationResult res = invoker.execute(request);
            //返回状态码
            if (res.getExitCode() == 0) {
                //打包成功
            } else {
                //打包失败
            }
        } catch (MavenInvocationException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取打包之后的jar包或者war包
     *
     * @param projectRootPath 项目根路径
     * @return
     */
    public static File getPackage(String projectRootPath) {
        //读取根路径
        File rootFile = new File(projectRootPath);
        //读取target目录
        File targetFile = new File(rootFile, "target");
        //根据正则匹配查找jar包或者war包
        File[] files = targetFile.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String grep = ".*\\.(war|jar)";
                return name.matches(grep);
            }
        });
        //返回找到的包 否则返回null
        return files != null ? files[0] : null;
    }
}
