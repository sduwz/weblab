package cn.sdu.weblab.util;

import java.io.*;


import java.util.zip.ZipEntry;
import java.util.zip.*;

/**
 * 解压zip工具类
 */
public class FileUtil {

    /**
     * zip解压
     *
     * @param inputFile 待解压文件路径+文件名
     * @param dirPath   解压路径
     */

    public static void ZipUncompress(String inputFile, String dirPath) throws Exception {
        File srcFile = new File(inputFile);//获取当前压缩文件
        // 判断源文件是否存在
        if (!srcFile.exists()) {
            throw new Exception(srcFile.getPath() + "所指文件不存在");
        }
        //开始解压
        //构建解压输入流
        ZipInputStream zin = new ZipInputStream(new FileInputStream(srcFile));
        ZipEntry entry = null;
        File file = null;
        while ((entry = zin.getNextEntry()) != null) {
            if (!entry.isDirectory()) {
                file = new File(dirPath, entry.getName());
                if (!file.exists()) {
                    new File(file.getParent()).mkdirs();//创建此文件的上级目录
                }
                OutputStream out = new FileOutputStream(file);
                BufferedOutputStream bos = new BufferedOutputStream(out);
                int len = -1;
                byte[] buf = new byte[1024];
                while ((len = zin.read(buf)) != -1) {
                    bos.write(buf, 0, len);
                }
                // 关流顺序，先打开的后关闭
                bos.close();
                out.close();
            }
        }
        System.out.println("解压完成");
    }

}

