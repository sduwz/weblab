package cn.sdu.weblab.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SubmitMapper {
    void submitByZip(@Param("userId") int userId, @Param("path") String path);
}
