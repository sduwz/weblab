package cn.sdu.weblab.mapper;

import cn.sdu.weblab.entity.Role;
import cn.sdu.weblab.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface UserMapper {

    @Insert("insert into users(user_id,user_name,user_pswd,login_email) values (#{id} ,#{username} ,#{password} ,#{email} )")
    void addUser(User user);

    User getUserByName(String name);

    List<Role> getRoleListByUserId(int id);

    @Insert("insert into user_role(user_id,role_id) values (#{userId},#{roleId})")
    void addAuthority(@Param("userId") int userId, @Param("roleId") int roleId);

    @Update("update users set user_pswd=#{password} where login_email =#{email} ")
    void updatePassword(@Param("email") String email, @Param("password") String password);
}
