package cn.sdu.weblab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User implements Serializable {
    protected Integer id;
    protected String username;
    protected String password;
    protected String email;
}
