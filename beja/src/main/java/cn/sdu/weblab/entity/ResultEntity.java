package cn.sdu.weblab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResultEntity {
    private Integer err;
    private String msg;
    private Object pkg;
    private Object dbg;

    public static ResultEntity success() {
        return new ResultEntity(0, "success", null, null);
    }

    public static ResultEntity success(Object pkg) {
        return new ResultEntity(0, "success", pkg, null);
    }

    public static ResultEntity error(String msg, Object dbg) {
        return new ResultEntity(-1, msg, null, dbg);
    }

    public static ResultEntity error() {
        return new ResultEntity(-1, "error", null, null);
    }
}
