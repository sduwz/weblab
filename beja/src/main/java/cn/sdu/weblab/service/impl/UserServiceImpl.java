package cn.sdu.weblab.service.impl;

import cn.sdu.weblab.entity.Role;
import cn.sdu.weblab.entity.SysUser;
import cn.sdu.weblab.entity.User;
import cn.sdu.weblab.mapper.UserMapper;
import cn.sdu.weblab.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userByName = userMapper.getUserByName(username);
        List<Role> roleList = userMapper.getRoleListByUserId(userByName.getId());
        SysUser sysUser = new SysUser(userByName, roleList);
        return sysUser;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addUser(User user) {
        // 给密码加密
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));
        // 添加用户
        userMapper.addUser(user);
        // 初始化角色权限 初始为学生
        userMapper.addAuthority(user.getId(), 1);
    }

    @Override
    public void updatePassword(String email, String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        password = encoder.encode(password);
        userMapper.updatePassword(email, password);
    }
}
