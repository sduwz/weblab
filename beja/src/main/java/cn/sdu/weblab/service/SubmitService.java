package cn.sdu.weblab.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface SubmitService {
    public void submitByZip(int userId, MultipartFile file) throws Exception;
}
