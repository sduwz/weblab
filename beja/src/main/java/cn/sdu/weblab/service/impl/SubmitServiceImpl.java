package cn.sdu.weblab.service.impl;

import cn.sdu.weblab.config.ConstVal;
import cn.sdu.weblab.mapper.SubmitMapper;
import cn.sdu.weblab.service.SubmitService;
import cn.sdu.weblab.util.FileUtil;
import cn.sdu.weblab.util.MavenBuild;
import cn.sdu.weblab.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

@Service
public class SubmitServiceImpl implements SubmitService {
    @Autowired
    private SubmitMapper submitMapper;

    @Override
    public void submitByZip(int userId, MultipartFile file) throws Exception {
        /**
         * 1、用户上传zip包
         * 2、接收zip包后写到磁盘
         * 3、解压缩
         * 4、打出jar包
         */
        // 写入文件
        String name = UUID.randomUUID().toString();
        String path = ConstVal.RECEIVE_ZIP_ROOT + ConstVal.SEPARATOR + name + ".zip";
        File writeFile = new File(path);
        writeFile.createNewFile();
        FileOutputStream out = new FileOutputStream(writeFile);
        byte[] bytes = file.getBytes();
        out.write(bytes);
        // 记录数据库
        submitMapper.submitByZip(UserUtil.getCurrentUser().getId(), path);
        // 解压缩
        String unzip = ConstVal.UN_ZIP_ROOT + ConstVal.SEPARATOR + name;
        File unzipdir = new File(unzip);
        unzipdir.mkdirs();
        FileUtil.ZipUncompress(path, unzip);
        String[] list = unzipdir.list();
        String s = list[0];
        // 开始打包
        MavenBuild.build(unzip + ConstVal.SEPARATOR + s, ConstVal.MAVEN_PATH);
    }
}
