package cn.sdu.weblab.service;

import cn.sdu.weblab.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    void addUser(User user);

    void updatePassword(String email, String password);
}
