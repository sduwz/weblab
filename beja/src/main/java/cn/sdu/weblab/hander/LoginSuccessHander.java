package cn.sdu.weblab.hander;

import cn.sdu.weblab.entity.ResultEntity;
import cn.sdu.weblab.entity.SysUser;
import cn.sdu.weblab.util.RedisUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

public class LoginSuccessHander extends SavedRequestAwareAuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        // 签发token
        SysUser sysUser = (SysUser) authentication.getPrincipal();
        sysUser.setPassword("");
        String token = UUID.randomUUID().toString();
        // 用户信息存入redis
        RedisUtil.setObject(token, sysUser, 6000);
        response.setHeader("access_token", token);
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSON(ResultEntity.success(token)));
    }
}
