package cn.sdu.weblab.filter;

import cn.sdu.weblab.entity.ResultEntity;
import cn.sdu.weblab.entity.SysUser;
import cn.sdu.weblab.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@Slf4j
public class AuthenticationFilter extends BasicAuthenticationFilter {
    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    public AuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationEntryPoint authenticationEntryPoint) {
        super(authenticationManager, authenticationEntryPoint);
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        try {
//            UsernamePasswordAuthenticationToken 继承 AbstractAuthenticationToken 实现 Authentication
//            所以当在页面中输入用户名和密码之后首先会进入到 UsernamePasswordAuthenticationToken验证(Authentication)，
            String token = "";
            token = request.getHeader("access_token");
            if (token == null || token.length() == 0) {
                response.setCharacterEncoding("UTF-8");
                PrintWriter writer = response.getWriter();
                writer.print(ResultEntity.error("请登录", null));
                return;
            }
            UsernamePasswordAuthenticationToken authentication = getAuthentication(token, response);
            if (authentication == null) {
                response.setCharacterEncoding("UTF-8");
                PrintWriter writer = response.getWriter();
                writer.print(ResultEntity.error("请登录", null));
                return;
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception e) {
            e.toString();
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token, HttpServletResponse response) throws IOException, ClassNotFoundException {
        SysUser sysUser = (SysUser) RedisUtil.getObject(token);
        if (sysUser == null) {
            return null;
        }
        RedisUtil.setObject(token, sysUser, 6000);
        return new UsernamePasswordAuthenticationToken(sysUser, null, sysUser.getAuthorities());
    }
}

