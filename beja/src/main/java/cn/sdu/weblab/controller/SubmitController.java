package cn.sdu.weblab.controller;

import cn.sdu.weblab.config.ConstVal;
import cn.sdu.weblab.entity.ResultEntity;
import cn.sdu.weblab.service.SubmitService;
import cn.sdu.weblab.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/submit")
public class SubmitController {
    @Autowired
    private SubmitService submitService;

    @PostMapping("/submitByZip")
    public ResultEntity submitByZip(MultipartFile file) throws Exception {
        if (file == null || file.isEmpty()) {
            return ResultEntity.error("文件为空", null);
        }
        submitService.submitByZip(UserUtil.getCurrentUser().getId(), file);
        return ResultEntity.success();

    }
}
