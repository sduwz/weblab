package cn.sdu.weblab.controller;

import cn.sdu.weblab.entity.ResultEntity;
import cn.sdu.weblab.entity.User;
import cn.sdu.weblab.service.UserService;
import cn.sdu.weblab.util.MailUtil;
import cn.sdu.weblab.util.RedisUtil;
import cn.sdu.weblab.util.UserUtil;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/register")
    public ResultEntity register(String email, String username, String password) throws Exception {
        // TODO: 2022/3/18 校验参数
        // 临时对象
        User user = new User(null, username, password, email);
        // 生成激活验证码
        String str = UserUtil.getVerifyCode(6);
        try {
            // 存入redis缓存
            RedisUtil.setObject(str, user, 6000);
            // 发送验证码
            MailUtil.sendSimpleMail(email, "验证码", str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResultEntity.success();
    }

    @PostMapping("/activation")
    public ResultEntity activation(String code) throws Exception {
        User user = (User) RedisUtil.getObject(code);
        if (user == null) {
            log.info("验证码无效或过期");
            return ResultEntity.error("验证码无效或过期", null);
        }
        // TODO: 2022/3/18 新增用户逻辑
        String str = UserUtil.getVerifyCode(6);
        user.setId(Integer.parseInt(str));
        userService.addUser(user);
        user.setPassword("");
        return ResultEntity.success(user);
    }

    @PostMapping("/logout")
    public ResultEntity logout(HttpServletRequest request) {
        String access_token = request.getHeader("access_token");
        RedisUtil.delete(access_token);
        return ResultEntity.success();
    }

    @PostMapping("/forget_password")
    public ResultEntity forgetPassword(String email) throws Exception {
        if (email == null || email.length() == 0) {
            return ResultEntity.error("邮箱不能为空", null);
        }
        String verifyCode = UserUtil.getVerifyCode(6);
        MailUtil.sendSimpleMail(email, "忘记密码", verifyCode);
        RedisUtil.setString(verifyCode, email, 600);
        return ResultEntity.success();
    }

    @PostMapping("/reset_password")
    public ResultEntity resetPassword(String code, String password) {
        if (code == null || password == null || code.length() == 0 || password.length() == 0) {
            return ResultEntity.error();
        }
        String email = RedisUtil.getString(code);
        userService.updatePassword(email, password);
        return ResultEntity.success();
    }

}
