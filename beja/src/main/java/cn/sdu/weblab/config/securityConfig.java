package cn.sdu.weblab.config;

import cn.sdu.weblab.filter.AuthenticationFilter;
import cn.sdu.weblab.filter.OptionsRequestFilter;
import cn.sdu.weblab.hander.LoginSuccessHander;
import cn.sdu.weblab.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class securityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;


    /**
     * 配置忽略掉的 URL 地址,一般用于js,css,图片等静态资源
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        //web.ignoring() 用来配置忽略掉的 URL 地址，一般用于静态文件
        web.ignoring().antMatchers("/js/**", "/css/**", "/fonts/**", "/images/**", "/lib/**")
                .antMatchers(
                        "/user/register",
                        "/user/activation",
                        "/favicon.ico",
                        "/user/register",
                        "/user/activation",
                        "/user/forget_password",
                        "/user/reset_password"
                );

    }

    /**
     * （认证）配置用户及其对应的角色
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //数据在内存中定义，一般要去数据库取，jdbc中去拿，
        auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());

    }

    /**
     * （授权）配置 URL 访问权限,对应用户的权限
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers(
                        "/user/register",
                        "/user/login",
                        "/user/activation",
                        "/favicon.ico",
                        "/user/register",
                        "/user/activation"
                )
                .permitAll();

        http.formLogin()
                .loginProcessingUrl("/user/login")
                .successHandler(new LoginSuccessHander())
                .and().csrf().disable()
                .cors().and().addFilterAfter(new OptionsRequestFilter(), CorsFilter.class);


        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new AuthenticationFilter(authenticationManager()));
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration().applyPermitDefaultValues();
        configuration.addExposedHeader("Authorization");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
