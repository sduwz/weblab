package cn.sdu.weblab.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MavenBuildTest {
    @Test
    public void build() {
        String proPath = "E:\\code\\java\\sdu-pta";
        String mvnPath = "D:\\ideaIU-2020.3.win\\plugins\\maven\\lib\\maven3";
        MavenBuild.build(proPath, mvnPath);
        MavenBuild.getPackage(proPath);
    }
}
