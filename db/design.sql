/* WebLab
 * Web实验室数据表设计
 */

/* 配置表
 * 
 * 相关固定的配置信息，非业务数据。
 * 约束的顺序是PK, FK, UK。先确保本表主键，再检查外键，最后确保本表内有效。
 * UK可能用到FK。
 * 
 * 配置记录一般较少，尽量用smallint字段。
 */

/* 身份证件类型
 */
create table identity_type(
  identity_type_id    smallint,
  identity_type_name  text,
  constraint identity_type_pk primary key(identity_type_id), -- 明确定义约束的优点是命名较规范。
  constraint identity_type_uk_name unique(identity_type_name)
);
comment on table identity_type is '身份证件类型';
comment on column identity_type.identity_type_id is '身份证件类型ID（主键）';
comment on column identity_type.identity_type_id is '身份证件类型名称';
insert into identity_type(identity_type_id, identity_type_name) values
(1, '居民身份证'),
(2, '港澳台居民居住证'),
(3, '学生证');

-- 性别表。参考：《个人基本信息分类和代码》GB/T 2261-2003
create table sex(
  sex_id    smallint,
  sex_name  text,
  constraint sex_pk primary key(sex_id), -- 明确定义约束的优点是命名较规范。
  constraint sex_uk_name unique(sex_name)
);
comment on table sex is '性别表';
comment on column sex.sex_id is '性别ID（主键）';
comment on column sex.sex_name is '性别';

insert into sex(sex_id, sex_name) values
(1, '男'), (2, '女');

/* 族群表（ethnicity）
 * 
 * 以文化特征区分的族群（而非以外貌等区分）
 * race则是生理特征。
 * nationality国籍
 */

/* 组织类型表
 * 大中小学、研究所、职业学校、公司等。
 */
create table org_type(
  org_type_id   smallint,
  org_type_name text,
  constraint org_type_pk primary key(org_type_id),
  constraint org_type_uk_name unique(org_type_name)
);
comment on table org_type is '组织类型表';
comment on column org_type.org_type_id is '组织类型ID（主键）';
comment on column org_type.org_type_name is '组织类型名称';

insert into org_type(org_type_id, org_type_name) values
(1, '大学'),
(2, '高中'),
(3, '初中'),
(4, '小学');

/* 成员类型表
 * 
 */
create table member_type(
  member_type_id     smallint,
  member_type_name   text,
  member_type_nm     text,
  constraint member_type_pk primary key(member_type_id),
  constraint member_type_uk_name unique(member_type_name),
  constraint member_type_uk_nm unique(member_type_nm)
);
comment on table member_type is '成员类型表';
comment on column member_type.member_type_id   is '成员类型ID（主键）';
comment on column member_type.member_type_name is '成员类型名称';
comment on column member_type.member_type_nm   is '成员类型简称';

insert into member_type(member_type_id, member_type_name, member_type_nm) values
(1, '教师', '教师'),
(2, '学生', '学生'),
(3, '管理人员', '管理'),
(4, '工程技术人员', '工程'),
(5, '实验技术人员', '实验');

/* 学生类型表
 * 
 * 编程竞赛从小学生到博士生都可能同台竞技。
 */
create table student_type(
  student_type_id     smallint,
  student_type_name   text,
  constraint student_type_pk primary key(student_type_id),
  constraint student_type_uk_name unique(student_type_name)
);
comment on table student_type is '学生类型表';
comment on column student_type.student_type_id   is '学生类型ID（主键）';
comment on column student_type.student_type_name is '学生类型名称';

insert into student_type(student_type_id, student_type_name) values
(1, '小学生'),
(2, '初中生'),
(3, '高中生'),
(4, '大学生'),
(5, '硕士生'),
(6, '博士生');

/* 邮箱类型表
 * 
 */
create table email_type(
  email_type_id     smallint,
  email_type_name   text,
  constraint email_type_pk primary key(email_type_id),
  constraint email_type_uk_name unique(email_type_name)
);
comment on table email_type is '邮箱类型表';
comment on column email_type.email_type_id   is '邮箱类型ID（主键）';
comment on column email_type.email_type_name is '邮箱类型名称';

insert into email_type(email_type_id, email_type_name) values
(1, '工作邮箱'),
(2, '私人邮箱');

/* 电话类型表
 * 
 */
create table phone_type(
  phone_type_id     smallint,
  phone_type_name   text,
  constraint phone_type_pk primary key(phone_type_id),
  constraint phone_type_uk_name unique(phone_type_name)
);
comment on table phone_type is '电话类型表';
comment on column phone_type.phone_type_id   is '电话类型ID（主键）';
comment on column phone_type.phone_type_name is '电话类型名称';

insert into phone_type(phone_type_id, phone_type_name) values
(1, '工作手机'),
(2, '私人手机'),
(3, '工作电话'),
(4, '家庭电话');

/* 评测类型表
 * 
 * 虽然填空和表达式形式一样，但填空是期望固定内容，而表达式则要结合上下文运行后才能确定
 * 片段题、接口题、程序题的共性是不能根据内容评判，而要根据运行结果评判。
 * 片段题有代码上下文，插入到固定位置后构成完整程序，再编译、评测。
 * 
 * 程序题就当成上下文为空？保存到固定的文件中？
 * 也允许指定文件名、包名、类名？
 * 
 * Web项目的评测：上传应用源码包，构建后在规定容器中运行。
 * 用Selenium测试前端元素。
 * 用RESTclient测试后端接口。
 */
create table judge_type(
  judge_type_id     smallint,
  judge_type_name   text,
  constraint judge_type_pk primary key(judge_type_id),
  constraint judge_type_uk_name unique(judge_type_name)
);
comment on table judge_type is '评判类型表';
comment on column judge_type.judge_type_id   is '评判类型ID（主键）';
comment on column judge_type.judge_type_name is '评判类型名称';

insert into judge_type(judge_type_id, judge_type_name) values
(1, '人工评判'),
(2, '自动对比'),
(3, '自动评测'),
(4, '特别评测');

/* 题目类型表
 * OJ平台的“题目”称为“Problem”
 * 不区分单选、多选。
 */
create table problem_type(
  problem_type_id     smallint,
  problem_type_name   text,
  constraint problem_type_pk primary key(problem_type_id),
  constraint problem_type_uk_name unique(problem_type_name)
);
comment on table problem_type is '题目类型表';
comment on column problem_type.problem_type_id   is '题目类型ID（主键）';
comment on column problem_type.problem_type_name is '题目类型名称';

insert into problem_type(problem_type_id, problem_type_name) values
(1, '选择题'), -- 客观题，机判。与答案对比评分。
(2, '填空题'), -- 客观题，机判。与答案对比评分。
(3, '判断题'), -- 客观题，机判。与答案对比评分。
(4, '问答题'), -- 主观题，人工评判。
(5, '编程题'), -- 客观题，机判。按测试结果评分。
(6, '应用题'); -- 客观题，机判。按测试结果评分。

-- 多选题：漏选有部分分吗？

/* 提交方式表
 * 上传文件（压缩包可支持多个源文件）、粘贴代码。
 */
create table submit_type(
  submit_type_id     smallint,
  submit_type_name   text,
  constraint submit_type_pk primary key(submit_type_id),
  constraint submit_type_uk_name unique(submit_type_name)
);
comment on table submit_type is '提交方式表';
comment on column submit_type.submit_type_id   is '提交方式ID（主键）';
comment on column submit_type.submit_type_name is '提交方式名称';

insert into submit_type(submit_type_id, submit_type_name) values
(1, '在线编辑'),
(2, '上传源码'),
(3, '上传程序'),
(4, '上传照片'),
(5, '上传视频');

/* 编程语言类型表
 * 入选标准：
 * * 该语言在其适用领域居于统治地位、优势明显。
 * * 不掌握它会极大限制综合能力，丧失一整类语言的能力。
 * 目标不是某种扩展名的代码，而是程序语言。
 * 不是开编程语言展览馆。
 */
create table lang(
  lang_id      smallint,
  lang_name    text,
  lang_ext     text,
  lang_desc    text,
  constraint lang_pk primary key(lang_id),
  constraint lang_uk_name unique(lang_name),
  constraint lang_uk_ext unique(lang_ext)
);
comment on table lang is '语言类型表';
comment on column lang.lang_id    is '编程语言ID（主键）';
comment on column lang.lang_name  is '编程语言名称';
comment on column lang.lang_ext   is '编程语言扩展名';
comment on column lang.lang_desc  is '编程语言描述';

insert into lang(lang_id, lang_name, lang_ext, lang_desc) values
(1, 'C', 'c', '系统开发。'), 
(2, 'C++', 'cpp', '高性能复杂应用开发、算法竞赛。'), -- cc
(3, 'Java', 'java', '商业应用开发。'), -- vs C#
(4, 'Python', 'py', '工具类应用开发、交互式研发。'), -- vs Ruby, Perl, R
(5, 'ECMAScript', 'es', 'Web开发。'),
(6, 'SQL', 'sql', '数据访问。'),
(7, 'Common Lisp', 'lisp', '函数式动态语言。'),
(8, 'Assembly', 's', '底层开发。');

/* 图片类型表
 */
create table image_type(
  image_type_id     smallint,
  image_type_name   text,
  image_ext         text,
  constraint image_type_pk primary key(image_type_id),
  constraint image_type_uk_name unique(image_type_name),
  constraint image_type_uk_ext unique(image_ext)
);
comment on table image_type is '图片类型表';
comment on column image_type.image_type_id    is '图片类型ID（主键）';
comment on column image_type.image_type_name  is '图片类型名称';
comment on column image_type.image_ext        is '图片扩展名';

insert into image_type(image_type_id, image_type_name, image_ext) values
(1, 'JPG', 'jpg'),
(2, 'PNG', 'png'),
(3, 'SVG', 'svg');

/* 视频类型表
 */
create table video_type(
  video_type_id     smallint,
  video_type_name   text,
  video_ext         text,
  constraint video_type_pk primary key(video_type_id),
  constraint video_type_uk_name unique(video_type_name),
  constraint video_type_uk_ext unique(video_ext)
);
comment on table video_type is '视频类型表';
comment on column video_type.video_type_id    is '视频类型ID（主键）';
comment on column video_type.video_type_name  is '视频类型名称';
comment on column video_type.video_ext        is '视频扩展名';

insert into video_type(video_type_id, video_type_name, video_ext) values
(1, 'MP4', 'mp4'),
(2, 'MKV', 'mkv');

/* 房间类型表
 * 数媒课设项目用
 */
create table room_type(
  room_type_id     smallint,
  room_type_name   text,
  constraint room_type_pk primary key(room_type_id),
  constraint room_type_uk_name unique(room_type_name)
);
comment on table room_type is '房屋类型表';
comment on column room_type.room_type_id    is '房屋类型ID（主键）';
comment on column room_type.room_type_name  is '房屋类型名称';

insert into room_type(room_type_id, room_type_name) values
(1, '办公室'),
(2, '会议室'),
(3, '研讨室'),
(4, '休息室'),
(5, '实验室'),
(6, '教室'),
(7, '自习室'),
(8, '阅览室');

/**********************************************************
 * 事务表
 * 与业务有关、与用户量相关，或依靠变动实现功能。
 * 
 * 此类表的数据可能涉及个人隐私或商业机密，不得放入源码仓库！
 *********************************************************/

/*  用户表
  user是PG的保留字，加s后缀回避。
  password也是PG的保留字，改为user_pswd。
  其实user_id、user_name也有风险。
*/
create table users(
  user_id       int,
  user_name     text,
  user_pswd     text,
  login_email   text,
  constraint user_pk primary key(user_id),
  constraint user_uk_name unique(user_name),
  constraint user_uk_email unique(login_email)
);
comment on table users is '用户表';
comment on column users.user_id      is '用户表ID（主键）';
comment on column users.user_name    is '用户名';
comment on column users.user_pswd    is '用户口令（加密后）';
comment on column users.login_email  is '登录邮箱';

/*  角色表
  role是PG的保留字，加s后缀回避。
*/
create table roles(
  role_id       int,
  role_name     text,
  role_desc     text,
  constraint role_pk primary key(role_id),
  constraint role_uk_name unique(role_name)
);
comment on table roles is '角色表';
comment on column roles.role_id    is '角色ID（主键）';
comment on column roles.role_name  is '角色名称';
comment on column roles.role_desc  is '角色描述';

/* 用户角色表 
*/
create table user_role(
  user_id       int,
  role_id       int,
  constraint user_role_pk primary key(user_id, role_id),
  constraint user_role_fk_user foreign key(user_id) references users(user_id),
  constraint user_role_fk_role foreign key(role_id) references roles(role_id)
);
comment on table user_role is '用户角色表。复合主键：(user_id,role_id)';
comment on column user_role.user_id  is '用户ID';
comment on column user_role.role_id  is '角色ID';

/* 组织机构表
 * 
 */
create table org(
  org_id        int,
  org_name      text,
  org_type_id   smallint,
  org_code      text, -- 组织机构代码
  constraint org_pk primary key(org_id),
  constraint org_fk_type foreign key(org_type_id) references org_type(org_type_id),
  constraint org_uk_name unique(org_name), -- 可能有重名
  constraint org_uk_code unique(org_code) -- 不考虑外国机构
);
comment on table org is '组织机构表';

/*
 * 校区表comment
 */
create table division(
  division_id     int,
  org_id          int,
  division_name   text,
  constraint division_pk primary key(division_id),
  constraint division_fk_org foreign key(org_id) references org(org_id),
  constraint division_uk_name unique(division_name)
);

/* 学部(faculty)
 * 
 */

/* 学院(school)
 * 
 */
create table school(
  school_id     int,
  school_name   text,
  school_nm     text, -- 简写
  org_id        int,
  constraint school_pk primary key(school_id),
  constraint school_fk_org foreign key(org_id) references org(org_id),
  constraint school_uk_name unique(org_id, school_name), -- 同组织内不重名
  constraint school_uk_nm unique(org_id, school_nm) -- 同组织内不重名
);
comment on table school is '学院表';

/* 系(department)
 */
create table department(
  department_id     int,
  department_name   text,
  school_id         int,
  constraint department_pk primary key(department_id),
  constraint department_fk_org foreign key(school_id) references school(school_id),
  constraint department_uk_name unique(school_id, department_name) -- 同院内不重名
);
comment on table department is '系表';

/* 专业表
 * 学生可以改专业、辅修专业。中小学不分专业。
 * 因此学生~专业设计成独立的关系。
 */
create table major(
  major_id      int,
  major_name    text,
  major_nm      text, -- 简写
  school_id     int,
  constraint major_pk primary key(major_id),
  constraint major_fk_school foreign key(school_id) references school(school_id),
  constraint major_uk_name unique(school_id, major_name), -- 同学院内专业不应重名
  constraint major_uk_nm unique(school_id, major_nm) -- 同学院内专业不应重名
);
comment on table major is '专业表';

/* 人员表
 * 主要保存稳定的信息。对于住址、联系方式等易变信息另行存储。
 * 存储隐私信息是要负责任的，尽量少存。
*/
create table person(
  person_id         int,
  person_name       text,
  sex_id            smallint,
  birth_date        date,
  identity_type_id  smallint,
  identity_code     text,
  constraint person_pk primary key(person_id),
  constraint person_fk_sex foreign key (sex_id) references sex(sex_id),
  constraint person_fk_idtype foreign key (identity_type_id) references identity_type(identity_type_id),
  constraint person_uk_id unique(identity_type_id, identity_code) -- 身份证件类型与编码需唯一
);
comment on table person is '人员表';

/* 邮件表
 * 
 */
create table email(
  email_id        int,
  email_address   text,
  person_id       int,
  email_type_id   smallint,
  main_flag       smallint,
  constraint email_pk primary key(email_id),
  constraint email_fk_person foreign key (person_id) references person(person_id),
  constraint email_fk_type foreign key (email_type_id) references email_type(email_type_id),
  constraint email_uk_address unique(email_address), -- 邮箱全局唯一
  constraint email_uk_main unique(person_id, main_flag) -- 同一人最多设一个主邮箱（无论工作或私人）
);
comment on table email is '邮件表';

/* 电话表
 * 
 */
create table phone(
  phone_id        int,
  phone_number    text,
  person_id       int,
  phone_type_id   smallint,
  main_flag       smallint,
  constraint phone_pk primary key(phone_id),
  constraint phone_fk_person foreign key (person_id) references person(person_id),
  constraint phone_fk_type foreign key (phone_type_id) references phone_type(phone_type_id),
  constraint phone_uk_address unique(phone_number), -- 电话号码全局唯一
  constraint phone_uk_main unique(person_id, main_flag) -- 同一人最多设一个主电话号码
);
comment on table phone is '电话表';

/* 组织成员表
 * 同一人可能在多所学校兼职。
 * 同一人可能在多个院系兼职。
 * 同一人可能从事不同性质的职务：教师、管理等。
 * 
 * 教师（teacher）不如员工（staff）通用，因为还有技术、管理、后勤等。
 * 员工不如成员（member）通用，因教师和学生都在学校，如果统一表达更方便。
 * 
 * member是SQL-2008关键字
 */
create table org_member(
  member_id       int,
  member_code     text, -- 组织内的编号，组织内唯一。
  person_id       int,
  org_id          int,
  member_type_id  smallint,
  enter_date      date, -- 加入日期
  leave_date      date, -- 离开日期。如毕业、退休、辞职等。同一人在同一学校，可能多次入学（大学、硕士）、入职。
  constraint member_pk primary key(member_id),
  constraint member_fk_person foreign key (person_id) references person(person_id), -- 需要先添加人员
  constraint member_fk_org foreign key (org_id) references org(org_id),
  constraint member_fk_member_type foreign key (member_type_id) references member_type(member_type_id),
  constraint member_uk_code unique(org_id, member_code)
);
comment on table org_member is '组织成员表';

/* 课程表
 * 课程名称：高级程序设计，学分4分
 * 上课：5-17周，周三3-4节，5-105 –周五5-6节（下午两点）， 5区-208
 * 实验：7-14周，周二16-18点，计算中心405
 * 课程成绩：平时成绩 （满分30）+ 期末成绩卷面（满分100） * 70%
 * 先修课程
 * 教学计划
 * 考试方式
 * 参考书目
 * 课件资源
 * 知识点
 * 章节
create table course(
  course_id     int,
  course_code   text,
  course_name   text,
);
comment on table course is '课程表';
 */

/* 学期
 * 
 */

/* 学院班级
 * 
 * 软件五年级1班、
create table school_class(
  school_class_id   int primary key,
  school_id         int,
  member_id          int
);
comment on table school_class is '学院班级表';
 */

/* 年级
 * 
 * 每过一学年全都要换。
 */

/* 专业班级
 * 
 * 级部~专业（文科、理科）
 * 
 * 历城一小五年级
 * 历城一中初三1班
 * 山大附中高二3班
 * 2021级软件工程专业8班
create table major_class(
  major_class_id    int,
  major_class_name  text,
  major_id          int,
);
comment on table major_class is '专业班级表';
 */

/* 课程班级表
 * 
 * 同一课程可能分成多个班，由不同老师教。
 * 
create table course_class(
  course_class_id     int primary key,
  course_id           int,
  teacher_member_id   int, -- 与教师对应吗？会有多位老师负责一个班？
);
comment on table course_class is '课程班级表';
 */

/* 课程注册表
 * 
 * 选课申请表（course_apply），成功再转到课程注册表（course_register）？
 * 限选可能选不上。
 * 选不够可能取消课程。
create table course_register(
  course_register   int,
  course_class_id   int,
  student_member_id         int, -- 冗余且有歧义的可能：教师与学生的org_id不同？
);
comment on table course_register is '课程注册表';
 */

/*
 * 课程班级成员表
 */
-- TODO create table course_class_member();

/* 课程作业
 * 
 * 作业、练习、实验、签到、模拟考试、考试。
create table course_task(
  course_task_id     int primary key,
  course_task_name   text not null,
  full_score  numeric(3) not null
);
comment on table course_task is '课程作业';
 */

/* 成绩表
 * 
create table course_score(
  course_score_id   bigint,
  student_id        int,
  task_id           int,
  score             numeric(6,2) not null,
  constraint web_score_pk primary key(student_id, task_id)
);
comment on table course_score is '成绩表';
 */

/*
 * 建筑物表
 * 数媒课设使用。
 */
create table building(
  building_id     int,
  division_id     int,
  building_name   text,
  constraint building_pk primary key(building_id),
  constraint building_fk_org foreign key (division_id) references division(division_id)
);

/*
 * 房间表
 * 数媒课设使用。
 */
create table room(
  room_id       int,
  building_id   int,
  room_type_id  smallint,
  room_name     text,
  constraint room_pk primary key(room_id),
  constraint room_fk_building foreign key (building_id) references building(building_id),
  constraint room_fk_type foreign key (room_type_id) references room_type(room_type_id)
);

/*
 * 房间利用表
 * 数媒课设使用。
 */
create table room_usage(
  room_id       int,
  seat_total    int not null,
  seat_used     int not null,
  update_time   timestamptz not null,
  constraint room_usage_pk primary key(room_id),
  constraint room_usage_fk_room foreign key (room_id) references room(room_id)
);
