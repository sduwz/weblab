package main

import (
  "fmt"
  "os"
  "database/sql"
  _ "github.com/lib/pq"
)

func main(){
  var dbname = "dbweblab"
  var dbusr = os.Getenv("WEBLAB_DB_USERNAME")
  var dbpwd = os.Getenv("WEBLAB_DB_PASSWORD")
  var dburl = fmt.Sprintf("postgres://%s:%s@127.0.0.1/%s?sslmode=disable", dbusr, dbpwd, dbname)

  db, err := sql.Open("postgres", dburl)
  if err != nil {
    panic(err)
  }

  var tms string
  err = db.QueryRow("select now() as tms").Scan(&tms)
  if err != nil {
    panic(err)
  }
  fmt.Printf("db_time: '%s'\n", tms)
}
