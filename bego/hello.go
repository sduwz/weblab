package main

import "github.com/gin-gonic/gin"

func main() {
  r := gin.Default()
  r.POST("/weblab/test/hello", func(c *gin.Context) {
    c.JSON(200, gin.H{
      "err": 0,
      "msg": "ok",
      "pkg": nil,
      "dbg": nil,
    })
  })
  r.Run()
}
