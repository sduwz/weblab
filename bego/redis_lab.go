package main

import (
  "context"
  "github.com/go-redis/redis/v8"
  "fmt"
)

var ctx = context.Background()

func check(rdb *redis.Client, k string) {
  v, err := rdb.Get(ctx, k).Result()
  if err == redis.Nil {
    fmt.Printf("key '%s' not found.\n", k)
  } else if err != nil {
    panic(err)
  } else {
    fmt.Printf("|%s| -> |%s|\n", k, v)
  }
}

func main() {
  rdb := redis.NewClient(&redis.Options{
    Addr:     "localhost:6379",
    Password: "",
    DB:       0,
  })

  err := rdb.Set(ctx, "pi", "3.14", 0).Err()
  if err != nil {
    panic(err)
  }

  check(rdb, "pi")
  check(rdb, "pwd")
}
