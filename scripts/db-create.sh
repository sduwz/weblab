#!/bin/bash

# run as postgres

if [ "$#" -lt 2 ]; then
  echo "usage: $0 <dbuser> <dbpswd> [dbnames...]"
  exit 1
fi

dbuser=$1
dbpswd=$2
echo "create role $dbuser login password '*'; commit;"
psql -c "create role $dbuser login password '$dbpswd'; commit;"

n=3
while [[ $n -le "$#" ]]; do
  dbname=${!n}
  echo "create database: $dbname"
  psql -c "create database $dbname with encoding 'UTF8' owner $dbuser template template0 lc_collate 'zh_CN.UTF-8' lc_ctype 'zh_CN.UTF-8';"
  psql -c "revoke connect on database $dbname from public; commit;"
  psql -c "grant connect on database $dbname to $dbuser; commit;"
  n=$((n + 1))
done
