#!/bin/bash

# run as postgres

for db in $*; do
  tms=$(date +%Y%m%d%H%M%S)
  fnm="$db-$tms.dmp"
  echo "dump $db to $fnm"
  pg_dump $db -f $fnm
done

