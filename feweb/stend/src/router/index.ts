import { createRouter, createWebHistory } from 'vue-router';
import codingViewVue from '../views/coding-view.vue';
import registerViewVue from '../views/register-view.vue';
import loginViewVue from '../views/login-view.vue';
import forgetpasswViewVue from '../views/forgetpassw-view.vue';
import updatepasswViewVue from '../views/updatepassw-view.vue';
import personalViewVue from '../views/personal-view.vue';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: loginViewVue,
      meta:{
        isLogin:false,
      }
    },
    {
      path: "/register",
      name: "register-view",
      component: registerViewVue,
      meta:{
        isLogin:false,
      }
    },
    {
      path: "/forget_password",
      name: "forgetpassw-view",
      component: forgetpasswViewVue,
      meta:{
        isLogin:false,
      }
    },
    {
      path: "/update_password",
      name: "update_password",
      component: updatepasswViewVue,
      meta:{
        isLogin:true,
      }
    },
    {
      path: "/coding",
      name: "coding",
      component:codingViewVue,
      meta:{
        isLogin:true,
      }
    },
    {
      path: "/personal",
      name: "personal",
      component:personalViewVue,
      meta:{
        isLogin:true,
      }
    },
  ],
});

export default router;
