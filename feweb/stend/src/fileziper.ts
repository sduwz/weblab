import JSZip from 'jszip';
import { saveAs } from 'file-saver';

enum FileType {
    root,
    folder,
    ts,
    js,
    html,
    css,
    md,
    txt,
}
const fileTypes = function (type: string): FileType | undefined {
    switch (type) {
        case "folder":
            return FileType.folder;
        case "ts":
            return FileType.ts;
        case "js":
            return FileType.js;
        case "html":
            return FileType.html;
        case "css":
            return FileType.css;
        case "md":
            return FileType.md;
        case "txt":
            return FileType.txt;
        default:
            return undefined
    }
}
class Ziper {
    projName: string = 'web';
    zip: any;
    head: any;
    file: any;
    constructor() {
        this.zip = new JSZip();
    }
    setProjectName(name: string) {
        this.projName = name;
    }
    updateProject(data: any, parent = this.zip) {
        if (parent == this.zip) {
            this.head = this.zip.folder(this.projName);
            parent = this.head;
        }
        for (var item in data) {
            if (data[item].type == FileType.folder) {
                let pa = this._addFolder(parent, data[item].name)
                this.updateProject(data[item].children, pa);
            } else {
                this._addFile(parent, data[item].name, data[item].value);
            }
        }
    }
    async uploadFile(): Promise<any> {
        let projName = this.projName + '.zip';
        const blob = await this.zip.generateAsync({ type: "blob" })
        var file = new File([blob], projName, { type: "zip" });
        let param = new FormData();
        param.append('file', file);
        saveAs(file)

        return new Promise((resolve, reject) => {
            resolve(param);
        });


    }
    _addFile(parent: any, name: string, value: string) {
        parent.file(name, value);
    }
    _addFolder(parent: any, name: string) {
        return parent.folder(name);
    }
    _saveZIP() {
        // this._fromZIP()
        let projName = this.projName;
        this.zip.generateAsync({ type: "blob" }).bind(this)
            .then((blob: any) => {
                // saveAs(blob, projName);
                // console.log(blob)
                this.file = blob;
            })
            .catch(() => {
                console.log("error")
                this.file = undefined;
            })
        return this.file;
    }
    _fromZIP() {

    }
}
const ziper = new Ziper();


export { ziper, FileType, fileTypes };