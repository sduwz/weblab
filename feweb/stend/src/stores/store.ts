import { defineStore } from "pinia";

const useLoginStore = defineStore({
  id: "login",
  state: () => ({
    isLogin: Number(localStorage.getItem('isLogin') || '0'),
    userName: localStorage.getItem('userName') || '',
    userPassword: localStorage.getItem('userPassword') || '',
    token: localStorage.getItem('token') || '',
    userId:localStorage.getItem('userId') || '',
    roles:localStorage.getItem('roles')||'',
  }),
  getters: {
    getIsLogin(state) {
      return (state.isLogin == 1) ? true : false;
    },
    getUserName(state) {
      return state.userName;
    },
    getUserPassword(state) {
      return state.userPassword;
    },
    getUserRoles(state){
      return state.roles;
    },
    getUserId(state){
      return state.userId;
    },

    getToken(state) {
      return state.token || '';
    },


  },
  actions: {
    userLogin(name: string, pwd: string, token: string,roles:string) {
      this.isLogin = 1;
      this.userName = name;
      this.userPassword = pwd;
      this.token = token;
      this.roles=roles;
      localStorage.setItem('isLogin', '1');
      localStorage.setItem('userId', name);
      localStorage.setItem('userPassword', pwd);
      localStorage.setItem('token', token);
      localStorage.setItem('roles',roles);
      console.log(token);
    },
    userLogout() {
      this.isLogin = 0;
      this.userName = '';
      this.userPassword = '';
      this.token = '';
      this.roles='';
      localStorage.removeItem('isLogin');
      localStorage.removeItem('userId');
      localStorage.removeItem('userPassword');
      localStorage.removeItem('token');
      localStorage.removeItem('roles');
      localStorage.removeItem('userId');
    },
    setUserId(id:string){
      this.userId=id;
      localStorage.setItem('userId',id);
    }

  },
});


export { useLoginStore};
