import { createRouter, createWebHistory } from 'vue-router';
import * as teacherhomevue from '../views/teacher-home.vue';
import registerViewVue from '../views/register-view.vue';
import loginViewVue from '../views/login-view.vue';
import forgetpasswViewVue from '../views/forgetpassw-view.vue';
import updatepasswViewVue from '../views/updatepassw-view.vue';
import teacherHomeVue from '../views/teacher-home.vue';
import personalViewVue from '@/views/personal-view.vue';
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "login",
      component: loginViewVue,
      meta: {
        isLogin: false,
      }
    },
    {
      path: "/register",
      name: "register-view",
      component: registerViewVue,
      meta: {
        isLogin: false,
      }
    },
    {
      path: "/forget_password",
      name: "forgetpassw-view",
      component: forgetpasswViewVue,
      meta: {
        isLogin: false,
      }
    },
    {
      path: "/update_password",
      name: "update_password",
      component: updatepasswViewVue,
      meta: {
        isLogin: true,
      }
    },
    {
      path: "/home",
      name: "home",
      component: teacherHomeVue,
      meta: {
        isLogin: true,
      },
    },
    {
      path: "/personal",
      name: "personal",
      component:personalViewVue,
      meta:{
        isLogin:true,
      }
    },
  ],
});

export default router;
