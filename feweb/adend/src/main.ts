import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import ElementPlus from "element-plus";
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/display.css'
import { instance} from './network/request'

import { useLoginStore } from "./stores/store";
const app = createApp(App);
app.use(createPinia());

app.use(ElementPlus);
app.use(router);

const store = useLoginStore();
// 添加请求拦截器
instance.interceptors.request.use(
    function (config) {
        if (config.headers!.access_token == '') {
            config.headers!.access_token = store.getToken;
        }
        return config;
    },
    function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

// 添加响应拦截器
instance.interceptors.response.use(
    function (response) {
        if (response.data.msg == undefined) {
            store.userLogout();
            router.push({ path: '/' })
        }
        return response;
    },
    function (error) {

        // 对响应错误做点什么
        return Promise.reject(error);
    }
);
router.beforeEach((to, from, next) => {
    //获取登录信息
    let isLogin = store.getIsLogin;

    if (isLogin) {     //已登录       
        if (!to.meta.isLogin) {
            next({
                path: '/home'
            })
        } else {
            next();
        }
    } else {    //未登录
        if (to.meta.isLogin) {
            next({
                path: '/',
            })
        } else {
            next()
        }
    }
});

app.mount('#app')
