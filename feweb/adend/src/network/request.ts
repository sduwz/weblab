import axios, { type AxiosRequestConfig } from 'axios'

const instance = axios.create({
    // baseURL: 'http://localhost:8080',
    // baseURL: 'http://211.87.224.233:8080',
    baseURL:'/api',
    timeout: 10000,
    method: 'post',
});

function request(url: string, params: any,token:string='') {
    return instance.post(url, params, {
        headers: {
            'Content-Type': 'multipart/form-data',
            'access_token':token
        },
        // transformRequest: [function (data) {
        //     var str = '';
        //     for (var key in data) {
        //         str += encodeURIComponent(key) + '=' + encodeURIComponent(data[key]) + '&';
        //     }
        //     return str;
        // }]
    });
}

export {request,instance}
